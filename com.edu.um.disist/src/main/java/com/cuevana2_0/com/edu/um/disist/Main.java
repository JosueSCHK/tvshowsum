package com.cuevana2_0.com.edu.um.disist;
//Iniciar servidor y alacenar error
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import com.cuevana2_0.com.edu.um.disist.errors.LogBotError;
import com.cuevana2_0.com.edu.um.disist.telegram.TvShowUm;

public class Main {
		
	public static void main(String[] args) throws Exception {
		LogBotError wError = new LogBotError();
		ApiContextInitializer.init();		
		TelegramBotsApi botsApi = new TelegramBotsApi();
		wError.LimpiaLog();
		try {				
			botsApi.registerBot(new TvShowUm());
			System.out.println("TvShowsUMBot successfully started");	
		} catch (TelegramApiException e) {
			wError.AlmacenarError(e.toString());
	    }			
	}		
}