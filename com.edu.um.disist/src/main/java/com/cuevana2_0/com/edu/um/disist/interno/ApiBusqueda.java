package com.cuevana2_0.com.edu.um.disist.interno;
//Se encarga de la busqueda en el Json Devuelve String
import com.cuevana2_0.com.edu.um.disist.info.OpcUser;
import com.cuevana2_0.com.edu.um.disist.json.LastEpisode;
import com.cuevana2_0.com.edu.um.disist.json.Next;
import com.cuevana2_0.com.edu.um.disist.json.TvMaze;
import com.google.gson.Gson;

public class ApiBusqueda {
	ReadURL reader =new ReadURL();
	Gson gson = new Gson();
	public String buscar(String busqueda, OpcUser muestra, Integer opc) {//Busqueda del Json principal
		String result=null;
		busqueda = busqueda.toString().replaceAll(" ", "-");
		busqueda="http://api.tvmaze.com/singlesearch/shows?q=" + busqueda;	
		TvMaze page;
		try {
			String json = reader.readUrl(busqueda);	
		    page = gson.fromJson(json, TvMaze.class);
		    switch (opc) {
				case 1://Traer Link del nuevo episodio				
				    result=page.getLinks().getNextepisode().getHref(); 
					break;
				case 2:// /search
					String link; 
					try {
							link = page.getLinks().getNextepisode().getHref();	
						}catch (Exception e) {
							link=null;
						}
					muestra.setLinkNext(link);
					try {
							link = page.getLinks().getPreviousepisode().getHref();
						}catch (Exception e) {
							link=null;
						}
						muestra.setLinkPrev(link);
						muestra.setId(page.getId());
						muestra.setLinkTvMaze(page.getUrl());
						result="Name: "+page.getName();
						if (page.getRating().getAverage()!=null) {
							result+="\nRating: "+page.getRating().getAverage();
						}
						result+="\n\nStatus: "+page.getStatus()+"\nRuntime: "+page.getRuntime()+" min."+"\n\n"+page.getSummary();
					break;
			}	    
		}catch (Exception e) {
			result="ERROR 254: Check name or try later";
			page=null;
		}
		return result;
	}
	
	public String buscarNextLast(String palabra, Integer opc) {
		String result=null;
		try {
			String json = reader.readUrl(palabra);
			switch (opc) {
			case 1: // Boton MoreInfo
				LastEpisode page1 = gson.fromJson(json, LastEpisode.class);
				result="Last Episode: Season " + page1.getSeason() + " - Episode " + page1.getNumber() + "\n'" + page1.getName()+"'";
				break;
			case 2: // Sig Episodio
				Next page = gson.fromJson(json, Next.class);
				result="Next Episode: Season " + page.getSeason() + " - Episode " + page.getNumber() + "\n'" + page.getName() +"'\nDay: "+ page.getAirdate();
			    if(!page.getAirtime().equals("")) {
			    	result+= " at " + page.getAirtime() + "hs";
		    }
		    	break;	
			}	       
		}catch (Exception e) {
			result="ERROR 254: Check name or try later";
		}
		return result;
	}
}