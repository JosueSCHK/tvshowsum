package com.cuevana2_0.com.edu.um.disist.telegram;
//Maneja el bot envio mjes
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.timedbot.TimedSendLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import com.cuevana2_0.com.edu.um.disist.errors.LogBotError;
import com.cuevana2_0.com.edu.um.disist.info.Usuarios;
import com.cuevana2_0.com.edu.um.disist.util.GetPropertiesValues;

public class TvShowUm extends TimedSendLongPollingBot{	
	
	Usuarios users = new Usuarios();// Almacenamosusuarios
	SuperUsuario sudo = new SuperUsuario();//Acciones del superusr
	Mensaje texto = new Mensaje();// Settear mensaje
	GetPropertiesValues botData;//Traer datos del .properties
	private long admin;
	LogBotError wError = new LogBotError();
	
	public TvShowUm () {
		botData = GetPropertiesValues.getInstance();//Carga los datos en el archivo
		admin = Long.parseLong(botData.getAdmin1());
	}
	public String getBotUsername() {
		return botData.getBotName();
	}
	public String getBotToken() {
		return botData.getToken();
	}
	
	public synchronized void onUpdateReceived(Update update) {
		SendMessage message = new SendMessage();
	   
		if (update.hasMessage() && update.getMessage().hasText()) {
	    	String messageText = update.getMessage().getText();
	    	long chatID = update.getMessage().getChatId();
	    	
	    	message=MjeAdmin(message, chatID, messageText);//ADMIN 
	    	if (messageText.equals("/admin")&&chatID==admin&&sudo.getAdmin()==0) {//Seteo del Mje
				message.setText("Hi admin, you are logged. You know what you want?");
				sudo.setAdmin(1);						
			}else {
				texto.SetVars(messageText,message,chatID, users);
				message=texto.MensajeTexto();
			}			
	    	sendTimed(chatID, message);
		}else if (update.hasCallbackQuery()) {//Boton presionado
		  	String callData = update.getCallbackQuery().getData();
		  	long chatID = update.getCallbackQuery().getMessage().getChatId();
         	message.setChatId(chatID);        	
     		message.setText(texto.botonMje(callData));
     		sendTimed(chatID, message);
	    	}
		}
	
	public SendMessage MjeAdmin(SendMessage message, long chatID, String messageText) {
    	message.setText(sudo.AdminCallBacks(messageText, chatID)).setChatId(admin);
		if (sudo.getAdmin()==1&&chatID==admin) {
			message.setText(sudo.Opciones(messageText,chatID, users));
    	}
    	try {
    		execute(message);
    	}catch (Exception e) {}
    	message.setChatId(chatID);
    	if (sudo.getMjeGlobal()!=null) {
    		message.setText(sudo.getMjeGlobal());
    		try {
	    		execute(message);
	    	}catch (Exception e) {}
		}
    	return message;
	}
	
	@SuppressWarnings("deprecation")
	public void sendMessageCallback(Long chatId, Object messageRequest){
		try{
			if (messageRequest instanceof SendMessage){
				sendMessage((SendMessage) messageRequest);
			}			
		}catch (TelegramApiException e){
			wError.AlmacenarError(e.toString());
		}catch (Exception e){
			wError.AlmacenarError(e.toString());
		}
	}
}