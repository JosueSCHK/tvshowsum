package com.cuevana2_0.com.edu.um.disist.json;
//Almacena link al json del nuevo y anterior episodio
public class Episode{
    private String href;

    public String getHref (){
        return href;
    }

    public void setHref (String href){
        this.href = href;
    }
}