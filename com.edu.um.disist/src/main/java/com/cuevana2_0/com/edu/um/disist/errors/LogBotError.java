package com.cuevana2_0.com.edu.um.disist.errors;
//Almacenamiento errores
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class LogBotError {
	
	public void AlmacenarError(String string) {
		FileWriter fw;
		LocalDateTime time = LocalDateTime.now();
		try {
			fw = new FileWriter("error.txt", true);
			PrintWriter writer = new PrintWriter(fw);//Sin sobrescribir
			writer.write(time+"\n"+string.toString()+"\n\t-------------------\n");
			writer.close();
			fw.close();
		} catch (IOException e2) {		
		}		
	}
	public void LimpiaLog() {
		try {
			PrintWriter cleaner = new PrintWriter("error.txt");
			cleaner.write("Log Errors:\n");
			cleaner.close();
			AlmacenarError("Last Update");
		}catch (IOException e) {
		}
	}
	
	public String MuestraContenido(){
	      String cadena=null, lector;
	      FileReader f;
		try {
			f = new FileReader("error.txt");
			BufferedReader b = new BufferedReader(f);
		    while((lector = b.readLine())!=null) {
			   cadena = cadena+"\n"+lector;
		    }
		    b.close();
		    f.close();
	        return cadena;
		} catch (FileNotFoundException e){
		} catch (IOException e2) {
		}
	     return "We can't show it";
	}
}
