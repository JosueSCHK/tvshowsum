package com.cuevana2_0.com.edu.um.disist.telegram;

import com.cuevana2_0.com.edu.um.disist.errors.LogBotError;
import com.cuevana2_0.com.edu.um.disist.info.Usuarios; 

public class SuperUsuario{
  private String texto=null;
  private Integer sniffer=0;
  private Integer admin=0;
  private Integer mensaje=0;
  private String mjeGlobal;
  LogBotError logFalla = new LogBotError();
  
  public Integer getAdmin() {
    return admin;
  }
  public void setAdmin(Integer admin) {
    this.admin = admin;
  }
  
  public String getMjeGlobal() {
    return mjeGlobal;
  }
  private void setMjeGlobal(String mjeGlobal) {
    this.mjeGlobal = mjeGlobal;
  }
  public String Opciones(String opcion,Long ID, Usuarios users) {
    texto="Sudo Active";
    switch (opcion) {
    case "Sniff":
      if (sniffer==0) {
        sniffer=1;
      }else {
        sniffer=0;
      }
      break;
    case "List":
      texto=texto+"\n"+users.print();
      break;
    case "ClearLst":
      users.borrar();
      break;
    case "ClearErr":
      logFalla.LimpiaLog();
      break;
    case "VerErr":
      texto=logFalla.MuestraContenido();
      break;
    case "TerminateEx":
      System.exit(0);
      break;
    case "MjeGlobal":
      if (mensaje==0) {
        mensaje=1;
      }else {
        mensaje = 0;
        setMjeGlobal(null);
      }
      break;
    case "No":  
      texto=texto+"\nAdmin List of Stuff:\n Sniff\n List\n ClearLst\n VerErr\n ClearErr\n MjeGlobal\n TerminateEx\n Exit";
      
      break;
    case "Exit":
      sniffer=0;
      this.setAdmin(0);
      break;
    default:
      if (mensaje==1) {
        setMjeGlobal(opcion);
      }
      break;
    }
    return texto;
  }
  
  public String AdminCallBacks (String msg, Long ID) {
    if(sniffer == 1) {
      texto="Sudo Active:\n"+ID+": "+msg;
    }else {
      texto="";
    }
    return texto;
  }
}