package com.cuevana2_0.com.edu.um.disist.info;
//Almacena variables usadas continuamente
public class OpcUser {
	private String linkNext;//Link al sig ep
	private String linkTvMaze;//Link al articulo de la pag
	private String linkPrev;//Link al anterior
	private String id;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLinkNext() {
		return linkNext;
	}
	public void setLinkNext(String linkNext) {
		this.linkNext = linkNext;
	}
	public String getLinkTvMaze() {
		return linkTvMaze;
	}
	public void setLinkTvMaze(String linkTvMaze) {
		this.linkTvMaze = linkTvMaze;
	}
	public String getLinkPrev() {
		return linkPrev;
	}
	public void setLinkPrev(String linkPrev) {
		this.linkPrev = linkPrev;
	}
}