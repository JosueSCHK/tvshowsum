package com.cuevana2_0.com.edu.um.disist.json;
//Link a cada json de los episodios
public class Links {
    private Episode previousepisode;
    private Episode nextepisode;
    
    public Episode getPreviousepisode (){
        return previousepisode;
    }

    public void setPreviousepisode (Episode previousepisode){
        this.previousepisode = previousepisode;
    }

    public Episode getNextepisode (){
        return nextepisode;
    }

    public void setNextepisode (Episode nextepisode){
        this.nextepisode = nextepisode;
    }

}
