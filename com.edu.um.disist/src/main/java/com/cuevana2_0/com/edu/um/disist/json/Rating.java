package com.cuevana2_0.com.edu.um.disist.json;
//Json del rating
public class Rating{
    private String average;

    public String getAverage (){
        return average;
    }

    public void setAverage (String average){
        this.average = average;
    }
}