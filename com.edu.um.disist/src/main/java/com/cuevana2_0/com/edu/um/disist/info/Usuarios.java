package com.cuevana2_0.com.edu.um.disist.info;
//Maneja los usuarios
import java.util.HashMap;

public class Usuarios {
	private HashMap<Long, Integer> users = new HashMap<>();
	
	public void agregar (Long id, Integer bandera) {
		//Agrega al usuario y/o actualiza su información
		users.put(id, bandera);	
	}
	
	public String print(){
		//Admin visualiza cantidad de usuarios
       return users.toString();
    }	
	
	public void borrar() {
		//Admin Limpia lista de usuarios
		users.clear();
	}
	
	public Integer obtenerDatos(Long id) {
		//Visualiza la bandera del usuario
		Integer result=0;
		if (users.containsKey(id)) {
	        result = users.get(id);
	    }else{
	    	agregar(id, 0);
	    	result=0;
	    }
		return result;
	}
}
