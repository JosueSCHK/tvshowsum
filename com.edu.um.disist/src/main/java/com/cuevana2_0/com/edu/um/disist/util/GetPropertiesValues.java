package com.cuevana2_0.com.edu.um.disist.util;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

public class GetPropertiesValues {
	private String botName;//Datos que son sensibles
	private String admin1;
	private String token;
	private static GetPropertiesValues instance = null;
	
	public String getBotName() { //Para poder agarrarlos desde otra clase
		return botName;
	}

	public String getAdmin1() {
		return admin1;
	}

	public String getToken() {
		return token;
	}

	InputStream inputStream;//Para poder abrir el archivo
 
	private GetPropertiesValues() { //Esta metodo agarra las variables codificadas y las almacena CODIFICADAS
 
		try {
			Properties prop = new Properties();
			String propFileName = "BotData.properties"; //Abro el archivo que contiene las variables
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			// get the property value and print it out
			botName = prop.getProperty("botName"); 
			token = prop.getProperty("token");
			admin1 = prop.getProperty("admin1");
			inputStream.close();
			DecryptValues();
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
	}
	
	private void DecryptValues() { //Este metodo DECODIFICA las variables que el metodo anterior extrajo del archivo
		Scanner usrInput = new Scanner(System.in);
		Decrypt values = new Decrypt(); //Esta es la clase de decodifica
		String passwd;
		System.out.println("Ingrese contraseña de servidor:\n");
		passwd = usrInput.nextLine();
		botName = values.Dec(getBotName(), passwd); 
		token = values.Dec(getToken(), passwd);
		admin1 = values.Dec(getAdmin1() , passwd);
		usrInput.close();
	}
	
	public static GetPropertiesValues getInstance() {
		if (instance == null) {
			instance = new GetPropertiesValues();
		}
		return instance;
	}
}
