package com.cuevana2_0.com.edu.um.disist.json;
//Informacion del ultimo episodio

public class LastEpisode{
	private String season;
	private String name;
	private String number;

	public String getSeason (){
		return season;
	}

	public void setSeason (String season){
		this.season = season;
	}

	public String getName (){
		return name;
	}

	public void setName (String name){
		this.name = name;
	}

	public String getNumber (){
		return number;
	}

	public void setNumber (String number){
		this.number = number;
	}

}