package com.cuevana2_0.com.edu.um.disist.telegram;
//Setea o recibe el texto del mensaje
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import com.cuevana2_0.com.edu.um.disist.info.OpcUser;
import java.util.ArrayList;
import java.util.List;
import com.cuevana2_0.com.edu.um.disist.info.Usuarios;
import com.cuevana2_0.com.edu.um.disist.interno.ApiBusqueda;

public class Mensaje {
	private OpcUser mostrarTV = new OpcUser();
	private String mje=null;
	private String messageText;
	private SendMessage message;
	private long chatID;
	private Usuarios users;
	
	public void SetVars(String texto, SendMessage mje, long id, Usuarios usr) {
		this.messageText=texto;
		this.message=mje;
		this.chatID=id;
		this.users=usr;
	}
	//TEXTO
	public SendMessage MensajeTexto() {
		switch (messageText) {//Usuario ingresa etiqueta
		case "/start":
			DefaultComp(0);
			message.setText("Commands you can use:\n /search to find information about a show\n /next to get the date of the next release\n /help if you need it");
			break;
		case "/help":
			DefaultComp(0);
			message.setText("Use /search and then the name of a TvShow to get the information about it\nor use /next to get the date of the next release");
			break;
		case "/about":
			DefaultComp(0);
			message.setText("Bot under construction by students of Computer Engineering in Argentina.\n\n" + 
					"If you find an error in the translation, answers or you want to make a suggestion, please inform in the following group:"+
					"https://t.me/joinchat/FYiCrw-olh-vE1I8gfZqIQ\n" + 
					"Update History:\n 0.9 Added Admin User\n 0.8 Constant information on buttons\n 0.7 ID for each User\n 0.6 Constant New Episode button"+
					"\n 0.5 Added /next\n 0.4 Added Buttons\n 0.3 Added /search\n 0.2 Added /start and /help\n 0.1 Starting Telegram Bot");
			break;
		case "/search":
	        DefaultComp(1);
	        message.setText("Write the name of the TvShow");
			break;
		case "/next":
    	    DefaultComp(2);
	        message.setText("Write the name of the TvShow");
			break;
		default://Usuario ingresa texto   	
		    	textIngresed();
			}
		return message;
	}
	private void textIngresed() {
		ApiBusqueda busqueda = new ApiBusqueda();
		switch (users.obtenerDatos(chatID)) {
			case 1://Search
		    	message.setText(busqueda.buscar(messageText, mostrarTV, 2));
		    	if (!message.getText().equals("ERROR 254: Check name or try later")) {
		    		message.setReplyMarkup(Keyboard(mostrarTV));//Teclado
				}
		    	DefaultComp(0);
				break;
			case 2://Next
				message.setText(busqueda.buscarNextLast(busqueda.buscar(messageText, mostrarTV, 1), 2));
				if (message.getText() == "ERROR 254: Check name or try later") {
					message.setText(messageText+" does not seem to have a new episode in our database.");
				}
				DefaultComp(0);
				break;
			default:
				DefaultComp(0);
				message.setText("Unknown command");
				break;
		}
	}
	
	private void DefaultComp (int bandera) {
		users.agregar(chatID, bandera);
		mostrarTV.setLinkNext(null);
		mostrarTV.setLinkPrev(null);
		mostrarTV.setLinkTvMaze(null);   
	}

//BOTONES
	public String botonMje(String callData) {
		ApiBusqueda buscar = new ApiBusqueda();
		String[] valores=callData.split("@");

        switch (valores[0]) {
			case "new_episode":
				mje=buscar.buscarNextLast(valores[1],2);
				break;
			case "more_info":
				mje=buscar.buscarNextLast(valores[1],1);
				mje=mje+"\n\nDescription Page:\n"+"http://www.tvmaze.com/shows/"+valores[2];
           	break;
			}		
		return mje;
	}
		
	private InlineKeyboardMarkup Keyboard(OpcUser mostrarTV) {
		InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(new InlineKeyboardButton().setText("More Info").setCallbackData("more_info@"+mostrarTV.getLinkPrev()+"@"+mostrarTV.getId()));
    	if(mostrarTV.getLinkNext()!=null) {
             rowInline.add(new InlineKeyboardButton().setText("New Episode").setCallbackData("new_episode@"+mostrarTV.getLinkNext()));
    	}	    	    	
	     rowsInline.add(rowInline);
	     markupInline.setKeyboard(rowsInline);
	     return markupInline;
	}
}