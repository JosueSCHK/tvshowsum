package com.cuevana2_0.com.edu.um.disist.json;
//Informacion nuevo episodio
public class Next extends LastEpisode{
    private String airtime;
    private String airdate;

    public String getAirtime (){
        return airtime;
    }

    public void setAirtime (String airtime){
        this.airtime = airtime;
    }

    public String getAirdate (){
        return airdate;
    }

    public void setAirdate (String airdate){
        this.airdate = airdate;
    }
}
			
	