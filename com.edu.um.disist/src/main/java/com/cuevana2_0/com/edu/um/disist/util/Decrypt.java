package com.cuevana2_0.com.edu.um.disist.util;

import java.security.Key;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Decrypt { //Le pasamos la palabra encriptada y la contraseña y devuelve sin encriptar
    public String Dec(String encrypted, String passwd) {
    	String salida;
    	try {
    		Cipher cipher = Cipher.getInstance("AES");
    		Key aesKey = new SecretKeySpec(passwd.getBytes(), "AES");
    		cipher.init(Cipher.DECRYPT_MODE, aesKey);
            salida = new String(cipher.doFinal(Base64.getDecoder().decode(encrypted)));
            
    	}catch(Exception e){
            salida="Error en la clave!\n";
        }
    	return salida;
    } 
}