package com.cuevana2_0.com.edu.um.disist.json;
//Principal del JSON
import org.jsoup.Jsoup;

public class TvMaze{
    private String summary;//Resumen
    private String status; //Estado de emision
    private Links _links; //Links
    private String runtime; //Tiempo por capitulo
    private String url;//Direccion de la pagina oficial de la serie en TvMaze
    private String id;
    private String name;//Nombre oficial
    private Rating rating;//Puntuacion de los usuarios
    
    public String getId (){
        return id;
    }

    public void setId (String id){
        this.id = id;
    }
    
    public String getSummary (){
    	return Jsoup.parse(summary).text();
    }

    public void setSummary (String summary){
        this.summary = summary;
    }

    public String getStatus (){
        return status;
    }

    public void setStatus (String status){
        this.status = status;
    }

    public Links getLinks (){
        return _links;
    }

    public void setLinks (Links Links){
        this._links = Links;
    }

    public String getRuntime (){
        return runtime;
    }

    public void setRuntime (String runtime){
        this.runtime = runtime;
    }

    public String getUrl (){
        return url;
    }

    public void setUrl (String url){
        this.url = url;
    }

    public String getName (){
        return name;
    }

    public void setName (String name){
        this.name = name;
    }

    public Rating getRating (){
        return rating;
    }

    public void setRating (Rating rating){
        this.rating = rating;
    }
}